package com.sakila.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.custom.backend.dto.CategoryDto;
import com.sakila.demo.interf.CategoryService;

@RestController
@ComponentScan({"com.sakila.demo.interf"})
public class CategoryController {

    @Autowired
    public CategoryService categoryService;

    @GetMapping(value="/categories", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<CategoryDto>> getCategories() {
        List<CategoryDto> categories = categoryService.categories();
        if(!CollectionUtils.isEmpty(categories)) {
            return new ResponseEntity<>(categories, HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<List<CategoryDto>>(categories, HttpStatus.NO_CONTENT);
    }
}
