
package com.sakila.demo.controller;

import com.custom.backend.dto.FilmDto;
import com.sakila.demo.interf.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@ComponentScan("com.sakila.demo.interf")
public class FilmController {

    @Autowired
    private FilmService filmService;

    @GetMapping(value="/films", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<FilmDto>> getFilm() {

        List<FilmDto> films = filmService.getFilms();
        if(!CollectionUtils.isEmpty(films)) {
            return new ResponseEntity<>(films, HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<List<FilmDto>>(films, HttpStatus.NO_CONTENT);
    }
}

