package com.sakila.demo.interf;

import com.custom.backend.dto.FilmDto;
import java.util.List;

public interface FilmService {

    public List<FilmDto> getFilms();
}
