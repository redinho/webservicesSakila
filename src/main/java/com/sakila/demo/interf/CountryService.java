package com.sakila.demo.interf;

import com.custom.backend.dto.CountryDto;
import java.util.List;

public interface CountryService {

    public List<CountryDto> getCountries();
}

