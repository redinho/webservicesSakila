package com.sakila.demo.service;

import com.custom.backend.dto.CategoryDto;
import com.custom.backend.service.interf.CategoryServiceSakila;
import com.sakila.demo.interf.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;


import java.util.List;

@Configuration
@Service("categoryService")
@ComponentScan(basePackages = {"com.custom.backend.service"})
public class CategoryServiceImpl  implements CategoryService {

    @Autowired
    public CategoryServiceSakila categoryServiceSakila;

    @Override
    public List<CategoryDto> categories() {
        return categoryServiceSakila.findAll();
    }
}
