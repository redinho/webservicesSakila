package com.sakila.demo.service;

import com.custom.backend.dto.CountryDto;
import com.custom.backend.service.interf.CountryServiceSakila;
import com.sakila.demo.interf.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("countryService")
public class CountryServiceImpl implements CountryService {

    @Autowired
    private CountryServiceSakila countryServiceSakila;

    public List<CountryDto> getCountries() {
        return  countryServiceSakila.getCountries();
    }
}
