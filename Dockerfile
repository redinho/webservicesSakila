# Alpine Linux with OpenJDK JRE
FROM openjdk:11-slim-buster

LABEL maintainer="reda20@sakila.com"

CMD mkdir /jar

COPY target/demo-1.0.0-RELEASE.jar /jar/

# port
EXPOSE 8080

# run application
ENTRYPOINT ["java", "-Dspring.profiles.active=dev", "-jar", "/jar/demo-1.0.0-RELEASE.jar"]
